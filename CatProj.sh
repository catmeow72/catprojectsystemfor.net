#!/bin/bash
source "$(dirname "$0")/ShellUtils.sh"
SafeSilentPopd() {
    if ! popd >/dev/null; then
        echo "Failed to go back to the previous directory." >&2
        echo "You may want to check the directory you are currently in because it is likely it has changed." >&2
        exit 1
    fi
}

SafeSilentPushd() {
    if ! pushd "$1" >/dev/null; then
        echo "Failed to go to directory "$1" because of the above error." >&2
        exit 1
    fi
}
if checkforprogram go; then
    SafeSilentPushd "$(dirname "$0")/Go"
    echo "Building generator..."
    if ! go build -o CatProj; then
        echo "Failed to build generator! See above error(s) for details." >&2
        SafeSilentPopd
        exit 1
    else
        echo "Done building generator!"
        SafeSilentPopd
    fi
fi
if [ -x "$(dirname "$0")/Go/CatProj" ]; then
    "$(dirname "$0")/Go/CatProj" "$@"
else
    echo "Error: The project system has not been build and cannot be built automatically either because Go is not installed, this project system is incompatible with the default version of Go installed, or there was an error compiling the file due to errors in the source files from errors created by the developer or a contributor." >&2
    echo "If Go is installed, you may want to check above for the output of the 'go build' command" >&2
    exit 1
fi

#!/bin/sh
export SHELLUTILS_DOWNLOADED=0
[ ! -f ./shellutils/ShellUtils.sh ] && export SHELLUTILS_DOWNLOADED=1
[ $SHELLUTILS_DOWNLOADED -eq 0 ] && echo "Downloading Catmeow Shell Utils as a Git submodule for the first time..."
export CATPROJECTSYSTEM_OLDDIR="`pwd`"
cd "`dirname $0`"
git submodule update --init
cd "$CATPROJECTSYSTEM_OLDDIR"
export CATPROJECTSYSTEM_OLDDIR=
if [ $SHELLUTILS_DOWNLOADED -eq 0 ]; then
	echo "DONE downloading Catmeow Shell Utils."
fi

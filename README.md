# Cat Project System For .NET

A project system written in Go for creating .NET projects.<br/>
Includes shell scripts to allow automatically building the project system if possible<br/>
The main use case of this is for creating multiple Nuget packages from one repository where all packages should use the same version.<br/>

## Getting started
If using git, put this in your project as a submodule.<br/>
Otherwise, clone this repository into your project.<br/>
Either way, to use it afterwards, run `CatProj.sh` which is inside the root of this repository to build (if Go is installed) and run the output.<br/>
You may pass command line arguments to this script to run certain subcommands with certain arguments.<br/>

### New projects
To make a new project:
1. Create a new directory with the desired name for the project
2. In a terminal, go to that directory
3. Run `git init` to initialize a git repository
4. Run `git submodule add https://gitlab.com/catmeow72/catprojectsystemfor.net.git ProjectSystem` to clone this repository as a submodule
5. Run `./ProjectSystem/CatProj.sh -c new`

### Existing projects
To add this to an existing project
1. In a terminal, go to the directory of the project
2. If using git for the project, run `git submodule add https://gitlab.com/catmeow72/catprojectsystemfor.net.git ProjectSystem`. If not, clone this repository as ProjectSystem in the root of the project
3. Run `./ProjectSystem/CatProj.sh -c init -add subproject1 -add subproject2, etc`

### Adding subprojects
The subproject adding commands should be run with the `-add subproject` argument and it can be specified multiple times to add multiple subprojects

#### Normal subprojects
Normal subprojects can be added with the `add` command

#### Tests
Tests can be added with the `addtest` command

### Testing, Nuget packages and Publishing
Testing can be done with the `test` command with no additional arguments and is also done before publishing.<br/>
Nuget packages are created with the `pack` command and also before publishing.<br/>
Publishing can be done with the `publish` command after you place an `ApiKey.txt` file containing a Nuget.org API key in the `ProjectConfig` directory at the root of the project<br/>

### Generating README.md and .csproj files with templates
A README.md file can be generated when there is both a README.md.tmpl and a CHANGELOG.md in the project's root directory.<br/>
README.md.tmpl is a Go template file in which .ChangeLog is the contents of CHANGELOG.md.<br/>
The command to use this feature is `readme`.<br/>
All projects can have .csproj.tmpl files which are Go Template files used for generating a .csproj file.<br/>
There should be a `Common` directory containing Go templates that define blocks of code for Nuget packages in .csproj files.<br/>
To use this feature, use the `csproj` command.<br/>

### Suplemental Makefile
This repository comes with a suplemental Makefile for easier use of the project system for those more familiar with Makefiles.<br/>
Note that for now this Makefile only supports some of the commands, and in order to set arguments, you must prefix `make` with `env ARGNAME=VALUE` where ARGNAME is the name of the argument and VALUE is the value to set it to.<br/>
If you try to run a command with required arguments without one or more of the required arguments, it will tell you the required arguments missing.<br/>

### Command Reference
See [COMMANDS.md](COMMANDS.md) for a detailed reference on all the commands included in this program and their arguments.

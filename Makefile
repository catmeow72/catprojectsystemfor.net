PROJECTSYSTEMDIR := ./ProjectSystem
.PHONY: default
default: default-warningmsg help

.PHONY: default-warningmsg
default-warningmsg:
	@echo WARNING: no command specified.
.PHONY: runalways
runalways:
	@$(PROJECTSYSTEMDIR)/Makefile_ProjectSystemDirCheck.sh || sh -c 'echo "ERROR: The project system directory was not valid. Check README.md within the directory this Makefile resides in (or the directory of the Makefile this symlink points to) for details."; exit 1'
	@$(PROJECTSYSTEMDIR)/DownloadShellUtils.sh
csproj: runalways
	@$(PROJECTSYSTEMDIR)/Makefile_CheckVariable.sh MANDATORY VERSION "$(VERSION)"
	$(PROJECTSYSTEMDIR)/CatProj.sh -c csproj -version "$(VERSION)"
publish: runalways
	$(PROJECTSYSTEMDIR)/CatProj.sh -c publish
pack: runalways
	$(PROJECTSYSTEMDIR)/CatProj.sh -c pack
pack-dev: runalways
	$(PROJECTSYSTEMDIR)/CatProj.sh -c packdev -debug
build: runalways
	$(PROJECTSYSTEMDIR)/CatProj.sh -c build
readme: runalways
	$(PROJECTSYSTEMDIR)/CatProj.sh -c readme
.PHONY: help
help: runalways
	@$(PROJECTSYSTEMDIR)/MakefileHelp.sh

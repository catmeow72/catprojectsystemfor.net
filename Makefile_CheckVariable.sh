#!/bin/sh
. "$(dirname "$0")/ShellUtils.sh"
if [ "$3" = "" ]; then
    printf "A "
    if [ "$1" = "MANDATORY" ]; then
        printf "%s" "mandatory"
    else
        printf "%s" "optional"
    fi
    printf " variable was not specified: %s.\n" "$2"
    exit 1
fi
# vim: set expandtab tabstop=4 shiftwidth=4:

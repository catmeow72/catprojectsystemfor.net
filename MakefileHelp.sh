#!/bin/sh
echo "Cat Project System For .NET Makefile Help" >&2
echo "Targets and Variables for those Targets: " >&2
echo "  csproj   Runs the csproj command of the project system." >&2
echo "    - VERSION: (Required) The -version argument for the csproj command" >&2
echo "  publish  Runs the publish command of the project system." >&2
echo "  pack     Runs the pack command of the project system." >&2
echo "  pack-dev Runs the packdev command of the project system with the -debug flag." >&2
echo "  build    Runs the build command of the project system." >&2
echo "  readme   Runs the readme command of the project system with the default output directory." >&2
echo "  help     Shows this help message" >&2
echo "" >&2
echo "For other commands, use CatProj.sh" >&2
echo "For more detailed help, use this help in conjunction with the COMMANDS.md file in the project system directory." >&2

package main

import (
	"bufio"
	"fmt"
	"os"
)

func AddLoop() {
	projectName := GetProjectName()
	Add(&projectsToAdd, projectName, false)
}
func AddTests() {
	projectName := GetProjectName()
	Add(&projectsToAdd, projectName, true)
}
func AddNoCheckLoop() {
	projectName := GetProjectName()
	for _, subproject := range projectsToAdd {
		AddNoCheck(subproject, projectName, false)
	}
}

func AddCheck(subprojects *projectsToAddType, projectName string, test bool) {
	fmt.Println("Preparing to add subprojects...")
	lastPathComponentBasename := "project"
	if test {
		lastPathComponentBasename = "test"
	}
	projectsFile, err := os.Open(projectConfigPath + "/" + lastPathComponentBasename + ".txt")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open project configuration file for reading: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)
	foundInFile := false
	for fileScanner.Scan() {
		for _, subproject := range *subprojects {
			if fileScanner.Text() == subproject {
				fmt.Printf("Notice: Subproject '%s' was not added to project '%s'\n", subproject, projectName)
				RemoveAllInstances(subprojects, subproject)
				foundInFile = true
				break
			}
		}
		if foundInFile {
			break
		}
	}

	projectsFile.Close()
	if foundInFile {
		AddCheck(subprojects, projectName, test)
	}
}

func AddCheckBasic(subprojects *[]string, projectName string, test bool) {
	var subprojectList projectsToAddType
	for _, subproject := range *subprojects {
		subprojectList.Set(subproject)
	}
	AddCheck(&subprojectList, projectName, test)
	*subprojects = subprojectList
}

func AddCheckSingle(subproject string, projectName string, test bool) (added bool) {
	var subprojects []string = make([]string, 1)
	subprojects[0] = subproject
	AddCheckBasic(&subprojects, projectName, test)
	added = len(subprojects) == 1
	return
}

func AddNoCheck(subprojectName, projectName string, test bool) {
	fmt.Printf("Adding subproject '%s' to project '%s'\n", subprojectName, projectName)
	lastPathComponentBasename := "project"
	if test {
		lastPathComponentBasename = "test"
	}

	projectsFile, err := os.OpenFile(projectConfigPath+"/"+lastPathComponentBasename+".txt", os.O_APPEND, 0644)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open project configuration file for writing: %s\n", err.Error())
		os.Exit(1)
	}
	projectsFile.WriteString(subprojectName)
	projectsFile.Close()
}

func Add(subprojects *projectsToAddType, projectName string, test bool) {
	AddCheck(subprojects, projectName, test)
	for _, subproject := range *subprojects {
		AddNoCheck(subproject, projectName, test)
	}
}

func AddBasic(subprojects *[]string, projectName string, test bool) {
	AddCheckBasic(subprojects, projectName, test)
	for _, subproject := range *subprojects {
		AddNoCheck(subproject, projectName, test)
	}
}

func AddSingle(subprojectName, projectName string, test bool) bool {
	if AddCheckSingle(subprojectName, projectName, test) {
		AddNoCheck(subprojectName, projectName, test)
		return true
	} else {
		return false
	}
}

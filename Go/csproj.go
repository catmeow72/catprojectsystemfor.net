package main

import (
	"bufio"
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"strings"
)

var VersionFlag string = ""

type CsprojDataStruct struct {
	Version string
}

var CsprojData CsprojDataStruct

func generateCsproj() {
	CsprojData = CsprojDataStruct{
		Version: VersionFlag,
	}
	fmt.Println("Loading templates for generating any csproj files in this project")
	var baseTemplates *template.Template = template.Must(template.ParseGlob(basePath + "/Common/*.tmpl"))
	projectsFile, err := os.Open(projectConfigPath + "/project.txt")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open project configuration file for reading: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		fmt.Printf("Loading the template for project '%s'\n", fileScanner.Text())
		csprojTemplates, err := filepath.Glob(basePath + "/" + fileScanner.Text() + "/*.csproj.tmpl")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to load template: %s\n", err.Error())
			os.Exit(1)
		}
		csprojTemplate := csprojTemplates[0]
		var templates *template.Template = template.Must(template.Must(baseTemplates.Clone()).ParseFiles(csprojTemplate))
		outputFile := strings.TrimSuffix(csprojTemplate, ".tmpl")
		fmt.Printf("Generating file '%s'\n", outputFile)
		file, err := os.Create(outputFile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to open file for writing: %s\n", err.Error())
			os.Exit(1)
		}
		var tmp = strings.Split(csprojTemplate, "/")
		var csprojTemplateForExecution = tmp[len(tmp)-1]
		err = templates.ExecuteTemplate(file, csprojTemplateForExecution, CsprojData)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to execute template: %s\n", err.Error())
			os.Exit(1)
		}
		file.Close()
	}

	projectsFile.Close()
}

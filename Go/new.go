package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/otiai10/copy"
)

func new() {
	var err error
	projectName := GetProjectName()
	err = os.Mkdir(basePath+"/"+projectName, os.ModeDir|os.ModePerm)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create directory for subproject files: %s\n", err.Error())
		os.Exit(1)
	}
	err = copy.Copy(appPath+"/template/Project/"+newTemplate, basePath+"/"+projectName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to copy templates for a specific subproject: %s\n", err.Error())
		os.Exit(1)
	}
	err = filepath.Walk(basePath+"/"+projectName, func(path string, info os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		if info.Mode().IsRegular() {
			var fileContents string
			var fileContentsBytes []byte
			fileContentsBytes, err = os.ReadFile(path)
			fileContents = string(fileContentsBytes)
			fileContents = regexp.MustCompile("ProjectName").ReplaceAllString(fileContents, projectName)
			fileContentsBytes = []byte(fileContents)
			os.WriteFile(path, fileContentsBytes, os.ModePerm)
		}
		return nil
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to walk directory completely: %s\n", err.Error())
		os.Exit(1)
	}
	if NugetFlag {
		err = copy.Copy(appPath+"/template/BuildFileTemplates/nuget/default.csproj.tmpl", basePath+"/"+projectName+"/"+projectName+".csproj.tmpl")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to copy templates for a Nuget enabled library: %s\n", err.Error())
			os.Exit(1)
		}
	} else {
		err = copy.Copy(appPath+"/template/BuildFileTemplates/"+newTemplate+"/default.csproj.tmpl", basePath+"/"+projectName+"/"+projectName+".csproj.tmpl")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to copy templates for a Nuget enabled library: %s\n", err.Error())
			os.Exit(1)
		}
	}
	initProject(false)
	AddNoCheck(projectName, projectName, false)
}

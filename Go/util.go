package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"unsafe"
)

// Splits a path into it's components, taking into account Windows-specific path separators when applicable
func SplitPath(path string) []string {
	var pathParts = strings.Split(path, "/")
	// Windows requires special behavior because it allows both '/' and '\' to be used as path separators.
	if runtime.GOOS == "windows" {
		var pathPartsTmp []string = make([]string, 0)
		for i := 0; i < len(pathParts); i++ {
			pathPartsOfParts := strings.Split(pathParts[i], "\\")
			for j := 0; j < len(pathPartsOfParts); j++ {
				pathPartsTmp = append(pathPartsTmp, pathPartsOfParts[j])
			}
		}
		pathParts = pathPartsTmp
	}
	return pathParts
}

// Gets the base path of the project
func GetBasePath() {
	basePath = "."
	for {
		var err error
		basePath, err = filepath.Abs(basePath)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to get the absolute path of '%s': %s\n", basePath, err.Error())
			os.Exit(1)
		}
		if _, err := os.Stat(basePath + "/ProjectConfig/project.txt"); err == nil {
			break
		} else if errors.Is(err, os.ErrNotExist) {
			basePath += "/.."
		} else {
			fmt.Fprintf(os.Stderr, "There is likely not a project here! (Error: %s)\n", err.Error())
			os.Exit(1)
		}
	}
}

func GetProjectName() string {
	projectPathParts := SplitPath(basePath)
	return projectPathParts[len(projectPathParts)-1]
}

func RemoveAllInstances(slice interface{}, toRemove interface{}) (err error) {
	// Use reflection to allow any type of slice pointer
	slicePtrVal := reflect.ValueOf(slice)
	if slicePtrVal.Kind() != reflect.Ptr {
		err = errors.New("pointer to slice required")
		return
	}
	sliceVal := slicePtrVal.Elem()
	if sliceVal.Kind() != reflect.Slice {
		err = errors.New("pointer to slice required")
		return
	}
	length := sliceVal.Len()

	// Caclulate the required length for the output
	newLength := 0
	for i := 0; i < length; i++ {
		if sliceVal.Index(i).Interface() != toRemove {
			newLength++
		}
	}
	// Create output
	out := make([]interface{}, newLength)
	// More complex for loop in other languages, but Go doesn't support for loops with multiple assignments, conditions, and/or iterations, so work around that by doing the work for the second variable manually.
	j := 0
	for i := 0; i < length; i++ {
		if sliceVal.Index(i).Interface() != toRemove {
			out[j] = sliceVal.Index(i)
			j++
		}
	}
	outVal := reflect.ValueOf(out)
	outVal = outVal.Convert(sliceVal.Type())
	slicePtrVal.SetPointer(unsafe.Pointer(outVal.Addr().Pointer()))
	return
}

func RemoveAllInstancesStr(slice *[]string, toRemove string) (err error) {
	length := len(*slice)

	// Caclulate the required length for the output
	newLength := 0
	for i := 0; i < length; i++ {
		if (*slice)[i] != toRemove {
			newLength++
		}
	}
	// Create output
	out := make([]string, newLength)
	// More complex for loop in other languages, but Go doesn't support for loops with multiple assignments, conditions, and/or iterations, so work around that by doing the work for the second variable manually.
	j := 0
	for i := 0; i < length; i++ {
		if (*slice)[i] != toRemove {
			out[j] = (*slice)[i]
			j++
		}
	}
	*slice = out
	return
}

func CheckForDuplicates(slice interface{}, toCheck interface{}) (duplicates bool, err error) {
	// Use reflection to allow any type of slice
	sliceVal := reflect.ValueOf(slice)
	if sliceVal.Kind() != reflect.Slice {
		err = errors.New("slice only")
		// Since the type of out isn't the same as the type of slice, a special case is needed.
		return
	}
	length := sliceVal.Len()
	for i := 0; i < length; i++ {
		for j := 0; j < length; j++ {
			if i != j {
				if sliceVal.Index(i) == sliceVal.Index(j) && sliceVal.Index(i).Interface() == toCheck {
					duplicates = true
					return
				}
			}
		}
	}
	duplicates = false
	return
}

func RemoveDuplicates(slice interface{}, toCheck interface{}) (err error) {
	// Use reflection to allow any type of slice pointer
	slicePtrVal := reflect.ValueOf(slice)
	if slicePtrVal.Kind() != reflect.Ptr {
		err = errors.New("pointer to slice required")
		return
	}
	sliceVal := slicePtrVal.Elem()
	if sliceVal.Kind() != reflect.Slice {
		err = errors.New("pointer to slice required")
		return
	}
	length := sliceVal.Len()
	var indexesToRemove []int = make([]int, 0)
	newLength := length
	for i := 0; i < length; i++ {
		for j := 0; j < length; j++ {
			if i != j {
				if sliceVal.Index(i) == sliceVal.Index(j) && sliceVal.Index(i).Interface() == toCheck {
					newLength--
					indexesToRemove = append(indexesToRemove, i)
					break
				}
			}
		}
	}
	newLength += 1
	indexesToRemove = indexesToRemove[1:]
	// Create output
	out := make([]interface{}, newLength)
	// More complex for loop in other languages, but Go doesn't support for loops with multiple assignments, conditions, and/or iterations, so work around that by doing the work for the second variable manually.
	j := 0
	for i := 0; i < length; i++ {
		for k := 0; k < len(indexesToRemove); k++ {
			if i != indexesToRemove[k] {
				out[j] = sliceVal.Index(i)
				j++
			}
		}
	}
	outVal := reflect.ValueOf(out)
	outVal = outVal.Convert(sliceVal.Type())
	slicePtrVal.SetPointer(unsafe.Pointer(outVal.Addr().Pointer()))
	return
}

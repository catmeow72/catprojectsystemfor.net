package main

import (
	"fmt"
	"os"
	"strings"
	"text/template"
)

type ReadmeDataStruct struct {
	ChangeLog   string
	Initialized bool
}

var ReadmeData ReadmeDataStruct = ReadmeDataStruct{
	Initialized: false,
}

func generateReadMe() {
	if !ReadmeData.Initialized {
		fmt.Println("Loading data for generating README.md in one or more directories")
		changelogBytes, err := os.ReadFile(basePath + "/CHANGELOG.md")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading CHANGELOG.md: %s\n", err.Error())
			os.Exit(1)
		}
		ReadmeData.ChangeLog = string(changelogBytes)
		ReadmeData.Initialized = true
	}
	fmt.Println("Loading templates for generating README.md in one or more directories")
	var templates *template.Template
	readmeTemplate := basePath + "/README.md.tmpl"
	templates = template.Must(template.Must(template.ParseGlob(basePath + "/Common/*.tmpl")).ParseFiles(readmeTemplate))
	var tmp = strings.Split(readmeTemplate, "/")
	var readmeTemplateForExecution = tmp[len(tmp)-1]
	var OutputPaths = strings.Split(OutputPathsStr, ":")
	for i := 0; i < len(OutputPaths); i++ {
		var OutputPath string = OutputPaths[i]
		fmt.Printf("Generating README.md for directory '%s'\n", OutputPath)
		file, err := os.Create(strings.TrimSuffix(OutputPath+"/"+readmeTemplateForExecution, ".tmpl"))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error creating README.md: %s\n", err.Error())
			os.Exit(1)
		}
		err = templates.ExecuteTemplate(file, readmeTemplateForExecution, ReadmeData)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to execute template: %s\n", err.Error())
			os.Exit(1)
		}
		file.Close()
	}
}

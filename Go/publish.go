package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

func publish() {
	packAll()
	testAll()
	fmt.Println("Publishing project to Nuget.org")
	apiKeyBytes, err := os.ReadFile(projectConfigPath + "/ApiKey.txt")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error retrieving API key. Make sure there is a file called 'ApiKey.txt' in '%s' with a Nuget.org API key. (Error: %s)", projectConfigPath, err.Error())
		os.Exit(1)
	}
	apiKey := string(apiKeyBytes)
	projectsFile, err := os.Open(projectConfigPath + "/project.txt")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening project list: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		pkgGlob := basePath + "/" + fileScanner.Text() + "/bin/"
		if DebugFlag {
			pkgGlob += "Debug"
		} else {
			pkgGlob += "Release"
		}
		pkgGlob += "/*.nupkg"
		path, err := filepath.Glob(pkgGlob)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't find a Nuget package file after packing: %s\n", err)
			os.Exit(1)
		}
		publishCmd := exec.Command("dotnet", "nuget", "push", path[0], "--api-key", apiKey, "--source", "https://api.nuget.org/v3/index.json")
		publishCmd.Stdout = os.Stdout
		publishCmd.Stderr = os.Stderr
		publishCmd.Stdin = os.Stdin
		publishCmd.Start()
		err = publishCmd.Wait()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to publish to Nuget.org\n")
			os.Exit(1)
		}
	}

	projectsFile.Close()
}

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var basePath string = ""
var projectConfigPath string = ""
var OutputPathsStr string = ""
var command string = ""
var newTemplate string = ""
var appPath string
var projectsToAdd projectsToAddType
var NugetFlag bool
var DebugFlag bool

type projectsToAddType []string

func (i *projectsToAddType) String() string {
	var output string = ""
	for _, project := range *i {
		output += project
		output += ":"
	}
	output = strings.TrimSuffix(output, ":")
	return output
}

func (i *projectsToAddType) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}
func main() {
	flag.StringVar(&command, "c", "help", "The command to execute (case insensitive). Possible values are 'help', 'add', 'addtest', 'csproj', 'readme', 'new', 'init', 'build', 'publish', 'pack', 'pack-dev', and 'test'. See COMMANDS.md for more information.")
	flag.StringVar(&basePath, "basepath", "", "Sets the base path. (defaults to the absolute path of the project, or for the new command, '.')")
	flag.StringVar(&OutputPathsStr, "o", "", "For the readme command, sets the output directory. Defaults to the base path. Add paths separated by colons to specify multiple paths.")
	flag.StringVar(&VersionFlag, "version", "", "Sets the version for the package(s). Required for the 'csproj' command.")
	flag.StringVar(&newTemplate, "template", "library", "Sets the template to be used for the 'new' command. Examples are 'library' for a library or 'application' for the most basic application possible.")
	flag.Var(&projectsToAdd, "add", "For commands 'add', 'addtest', and 'init', adds a project to the projects or tests list. You may specify this flag as many times as you like to add varying amounts of subprojects to the list.")
	flag.BoolVar(&NugetFlag, "nuget", false, "For the 'new' command, creates a Nuget-enabled project and as a result allows the Nuget-related commands to do something useful.")
	flag.BoolVar(&DebugFlag, "debug", false, "For the 'pack', 'packdev', and 'publish' commands, enables creating packages using the debug configuration (Useful for more easily debugging a package).")
	var HelpFlag bool
	flag.BoolVar(&HelpFlag, "help", false, "Shows this help message.")
	flag.Parse()
	var err error

	appPath, err = os.Executable()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to get app executable: %s\n", err.Error())
		os.Exit(1)
	}
	appPath, err = filepath.Abs(appPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to get the absolute path of '%s': %s\n", basePath, err.Error())
		os.Exit(1)
	}
	appPath = filepath.Dir(appPath)
	if HelpFlag {
		flag.CommandLine.Usage()
		os.Exit(0)
	}
	if basePath == "" {
		if strings.ToLower(command) == "new" {
			basePath, err = filepath.Abs(".")
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to get the absolute path of '%s': %s\n", basePath, err.Error())
				os.Exit(1)
			}
		} else {
			GetBasePath()
		}
	}
	if OutputPathsStr == "" {
		OutputPathsStr = basePath
	}
	projectConfigPath = basePath + "/ProjectConfig"
	switch strings.ToLower(command) {
	case "help":
		flag.CommandLine.Usage()
		os.Exit(0)
	case "test":
		testAll()
	case "addtest":
		AddTests()
	case "add":
		AddLoop()
	case "pack":
		packAll()
	case "init":
		initProject(true)
		AddNoCheckLoop()
	case "new":
		new()
	case "csproj":
		if VersionFlag == "" {
			flag.CommandLine.Usage()
			os.Exit(1)
		}
		generateCsproj()
	case "readme":
		generateReadMe()
	case "build":
		buildAll()
	case "publish":
		publish()
	case "packdev":
		packDev()
	default:
		fmt.Fprintf(os.Stderr, "Invalid command: %s\n", strings.ToLower(command))
		flag.CommandLine.Usage()
		os.Exit(1)
	}
}

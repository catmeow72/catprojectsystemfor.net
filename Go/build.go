package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

func buildAll() {
	fmt.Println("Building project...")
	projectsFile, err := os.Open(projectConfigPath + "/project.txt")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening project list: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		buildProject(fileScanner.Text())
	}

	projectsFile.Close()
}

func buildProject(project string) {
	prevDir, err := filepath.Abs(".")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't get current working directory: %s\n", err.Error())
		os.Exit(1)
	}
	os.Chdir(basePath + "/" + project)
	buildCmd := exec.Command("dotnet", "build")
	buildCmd.Stdout = os.Stdout
	buildCmd.Stderr = os.Stderr
	buildCmd.Stdin = os.Stdin
	buildCmd.Start()
	err = buildCmd.Wait()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Dotnet build failed!")
		os.Exit(1)
	}
	os.Chdir(prevDir)
}

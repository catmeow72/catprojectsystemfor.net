package main

import (
	"fmt"
	"os"

	"github.com/otiai10/copy"
)

func initProject(Required bool) {
	err := copy.Copy(appPath+"/Templates/README.md.tmpl", basePath)
	if err != nil && Required {
		fmt.Fprintf(os.Stderr, "Failed to copy README.md template: %s\n", err.Error())
		os.Exit(1)
	}
	if err != nil {
		return
	}
	err = copy.Copy(appPath+"/Templates/CHANGELOG.md", basePath)
	if err != nil && Required {
		fmt.Fprintf(os.Stderr, "Failed to copy CHANGELOG.md template: %s\n", err.Error())
		os.Exit(1)
	}
	if err != nil {
		return
	}
	newgitignore, err := os.ReadFile(appPath + "/Templates/.gitignore")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couln't open and read .gitignore additions: %s\n", err)
		os.Exit(1)
	}
	gitignore, err := os.OpenFile(basePath+"/.gitignore", os.O_APPEND, 0644)
	if err != nil && Required {
		fmt.Fprintf(os.Stderr, "Couln't open .gitignore: %s\n", err)
		os.Exit(1)
	}
	if err != nil {
		return
	}
	gitignore.WriteString("\n")
	gitignore.Write(newgitignore)
	gitignore.Close()

	err = copy.Copy(appPath+"/template/Common", basePath)
	if err != nil && Required {
		fmt.Fprintf(os.Stderr, "Failed to copy templates to be used in all projects: %s\n", err.Error())
		os.Exit(1)
	}
	if err != nil {
		return
	}
	generateReadMe()
}

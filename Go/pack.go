package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"unicode/utf8"

	"github.com/otiai10/copy"
)

func packAll() {
	fmt.Println("Generating Nuget packages...")
	projectsFile, err := os.Open(projectConfigPath + "/project.txt")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening project list: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		packProject(fileScanner.Text())
	}

	projectsFile.Close()
}

func packProject(project string) {
	prevDir, err := filepath.Abs(".")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't get current working directory: %s\n", err.Error())
		os.Exit(1)
	}
	os.Chdir(basePath + "/" + project)
	os.RemoveAll("bin")

	var BuildConfig string
	if DebugFlag {
		BuildConfig = "Debug"
	} else {
		BuildConfig = "Release"
	}
	buildCmd := exec.Command("dotnet", "pack", "-c", BuildConfig)
	buildCmd.Stdout = os.Stdout
	buildCmd.Stderr = os.Stderr
	buildCmd.Stdin = os.Stdin
	buildCmd.Start()
	err = buildCmd.Wait()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Dotnet build failed!")
		os.Exit(1)
	}
	os.Chdir(prevDir)
}

func packDev() {
	if !DebugFlag {
		fmt.Fprintf(os.Stderr, "WARNING: Since this command creates a package repository in a local folder for development, it is recommended to enable the -debug option.")
	}
	packAll()
	nugetDevOutputDir := basePath + "/NugetDevOutput"
	os.Mkdir(nugetDevOutputDir, os.ModeDir|os.ModePerm)
	projectsFile, err := os.Open(projectConfigPath + "/project.txt")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening project list: %s\n", err.Error())
		os.Exit(1)
	}
	fileScanner := bufio.NewScanner(projectsFile)

	fileScanner.Split(bufio.ScanLines)
	for fileScanner.Scan() {
		inputDir := basePath + "/" + fileScanner.Text() + "/bin/"
		if DebugFlag {
			inputDir += "Debug/"
		} else {
			inputDir += "Release/"
		}
		path, err := filepath.Glob(inputDir + "*.nupkg")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't find a Nuget package file after packing: %s\n", err)
			os.Exit(1)
		}
		symbolsPath, err := filepath.Glob(inputDir + "*.snupkg")
		if err != nil {
			fmt.Fprintf(os.Stderr, "WARNING: Couldn't find a Nuget symbols package: %s\n", err)
		}
		copy.Copy(path[0], nugetDevOutputDir+"/"+path[0][utf8.RuneCountInString(inputDir):])
		if err == nil {
			copy.Copy(path[0], nugetDevOutputDir+"/"+symbolsPath[0][utf8.RuneCountInString(inputDir):])
		}
	}
}

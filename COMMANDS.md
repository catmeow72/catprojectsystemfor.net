# Cat Project System for .NET Commands
## Command argument
This project system has an option to specify a command: -c<br/>
It takes one argument: the command to use
## Arguments for commands
|         Command(s)            |  Option   |                                  Description                                   |
|-------------------------------|-----------|--------------------------------------------------------------------------------|
| csproj, publish               | -version  | The version to use when generating .csproj files.                              |
| add, addtest, init            | -add      | A project to add. May be specified multiple times to add multiple subprojects. |
| All commands                  | -basepath | The base path of the project                                                   |
| readme                        | -o        | One or more output directories, separated by colons.                           |
| pack, packdev, publish        | -debug    | Uses the debug configuration of the project                                    |
| new                           | -template | The template to use                                                            |
| new                           | -nuget    | Enables creating a Nuget-enabled subproject                                    |
| (Command option not required) | -help     | Shows a help message.                                                          |
## Commands
| Command | Required option(s) |                                            Description                                                |
|---------|--------------------|-------------------------------------------------------------------------------------------------------|
| help    | None               | Shows a help message                                                                                  | 
| add     | -add               | Adds subproject(s)                                                                                    |
| addtest | -add               | Adds test(s)                                                                                          |
| csproj  | -version           | Updates .csproj files                                                                                 |
| readme  | None               | Updates a README.md file                                                                              |
| new     | -template          | Creates a new subproject, and initializes the project if necessary.                                   |
| build   | None               | Builds all subprojects                                                                                |
| publish | None               | Publishes to Nuget.org using the ApiKey.txt file in the ProjectConfig directory within the project    |
| pack    | None               | Builds all subprojects as Nuget packages                                                              |
| packdev | None               | Builds all subprojects as Nuget packages, then puts the packages in a directory called NugetDevSource |
| test    | None               | Runs all tests                                                                                        |
| init    | None               | Initializes a project                                                                                 |
## Additonal information on specific commands
### csproj
This command requires a .csproj.tmpl file to be present in each subproject.<br/>
If there are no .csproj.tmpl files referencing .Version (Even by including .NugetCommon), the -version argument won't do anything<br/>
### publish
This command also runs the test and pack commands internally to test the project and build the NuGet packages respectively.<br/>

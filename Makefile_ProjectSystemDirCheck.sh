#!/bin/sh
# This script only exists to make sure the project system directory specified in the Makefile actually points to the Cat Project System for .NET
# If it doesn't, this script won't be found and the Makefile will detect an error.
exit 0
